FROM nginx:stable
 
WORKDIR /etc/nginx/conf.d
 
COPY nginx/nginx.conf .

RUN mv nginx.conf default.conf
# apt-get update
# apt-get install snapd
# apt-get install certbot
# apt-get install python3-certbot-nginx
# certbot --nginx

WORKDIR /var/www/html

RUN chown -R www-data:www-data .

COPY src .