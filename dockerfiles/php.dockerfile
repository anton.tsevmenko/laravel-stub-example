FROM phpdockerio/php80-fpm:latest
#php:7.4-fpm-alpine

WORKDIR /var/www/html

RUN chown -R www-data:www-data .

COPY src .